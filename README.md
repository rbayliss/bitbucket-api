PHP Bitbucket API Client
========================

This library implements the Bitbucket V2 API using the Guzzle HTTP library.

It does not currently attempt to implement any of the V1 features.

Usage:
---------
```
#!php
// Access the Bitbucket APIs without authentication
$api = new \rbayliss\BitbucketAPI\Bitbucket();

// Access the APIs using username/password authentication:
$api = new \rbayliss\BitbucketAPI\Bitbucket([
  'login' => [
    'yourusername',
    'yourpassword',
  ]
]);

// Access the APIs using OAuth authentication:
$api = new \rbayliss\BitbucketAPI\Bitbucket([
  'oauth' => [
    'key' => 'YOURKEY',
    'secret' => 'YOURSECRET',
  ]
]);
```

This project implements all of the V2 Bitbucket endpoints.  Those endpoints are:

* *Users* - $api->users()
* *Teams* - $api->teams()
* *Repositories* - $api->repositories()
* *Commits* - $api->commits()
* *Branch Restrictions* - $api->branchRestrictions()