<?php

namespace rbayliss\BitbucketAPI\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Command\Guzzle\GuzzleClient;
use rbayliss\BitbucketAPI\Bitbucket;
use rbayliss\BitbucketAPI\BitbucketClient;

class BranchRestrictionsClientTest extends AbstractClientTest {

  public function getClient() {
    $description = Bitbucket::getDescription('branch-restrictions');
    $client = new Client();
    return new BitbucketClient($client, $description);
  }

  public function testGetBranchRestrictions() {
    $command = $this->client->getCommand('getBranchRestrictions', ['owner' => 'foo', 'repo_slug' => 'bar']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/branch-restrictions', $command);
  }

  public function testGetBranchRestriction() {
    $command = $this->client->getCommand('getBranchRestriction', ['owner' => 'foo', 'repo_slug' => 'bar', 'restriction_id' => 123]);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/branch-restrictions/123', $command);
  }

  public function testCreateBranchRestriction() {
    $restriction = [
      'kind' => 'push',
      'pattern' => 'example',
      'users' => [
        ['username' => 'test']
      ]
    ];
    $command = $this->client->getCommand('createBranchRestriction', [
      'owner' => 'foo',
      'repo_slug' => 'bar',
    ] + $restriction);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/branch-restrictions', $command);
    $this->assertCommandMethod('POST', $command);
    $this->assertCommandBody(json_encode($restriction), $command);
  }

  public function testUpdateBranchRestriction() {
    $restriction = [
      'groups' => [
        ['name' => 'test'],
      ],
      'users' => [
        ['username' => 'test']
      ],
      'pattern' => 'example',
    ];
    $command = $this->client->getCommand('updateBranchRestriction', [
        'owner' => 'foo',
        'repo_slug' => 'bar',
        'restriction_id' => 1,
      ] + $restriction);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/branch-restrictions/1', $command);
    $this->assertCommandMethod('PUT', $command);
    $this->assertCommandBody(json_encode($restriction), $command);
  }

  public function testDeleteBranchRestriction() {
    $command = $this->client->getCommand('deleteBranchRestriction', [
      'owner' => 'foo',
      'repo_slug' => 'bar',
      'restriction_id' => 1,
    ]);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/branch-restrictions/1', $command);
    $this->assertCommandMethod('DELETE', $command);
  }
}