<?php

namespace rbayliss\BitbucketAPI\Tests;

use GuzzleHttp\Client;
use rbayliss\BitbucketAPI\Bitbucket;

class BitbucketTest extends \PHPUnit_Framework_TestCase {

  public function testServicesExist() {
    $bitbucket = new Bitbucket();
    $this->assertInstanceOf('GuzzleHttp\Command\Guzzle\GuzzleClient', $bitbucket->users());
    $this->assertInstanceOf('GuzzleHttp\Command\Guzzle\GuzzleClient', $bitbucket->teams());
    $this->assertInstanceOf('GuzzleHttp\Command\Guzzle\GuzzleClient', $bitbucket->repositories());
    $this->assertInstanceOf('GuzzleHttp\Command\Guzzle\GuzzleClient', $bitbucket->commits());
    $this->assertInstanceOf('GuzzleHttp\Command\Guzzle\GuzzleClient', $bitbucket->branchRestrictions());
  }

  public function testHttpClientOverride() {
    $client = new Client();
    $bitbucket = new Bitbucket();
    $bitbucket->setHttpClient($client);
    $this->assertSame($client, $bitbucket->users()->getHttpClient());
  }

  public function testGlobalOptions() {
    $bitbucket = new Bitbucket(['defaults' => ['foo' => 'bar']]);
    $this->assertEquals('bar', $bitbucket->users()->getConfig('defaults/foo'));
  }

  public function testServiceOptions() {
    $bitbucket = new Bitbucket(['defaults' => ['foo' => 'bar']]);
    $users = $bitbucket->users(['defaults' => ['foo' => 'baz']]);
    $this->assertEquals('baz', $users->getConfig('defaults/foo'));
  }

  public function testAddCredentials() {
    $bitbucket = new Bitbucket(['login' => ['username' => 'foo', 'password' => 'bar']]);
    $auth = $bitbucket->users()->getHttpClient()->getDefaultOption('auth');
    $this->assertEquals(['foo', 'bar'], $auth);
  }
}