<?php

namespace rbayliss\BitbucketAPI\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Command\Guzzle\GuzzleClient;
use rbayliss\BitbucketAPI\Bitbucket;
use rbayliss\BitbucketAPI\BitbucketClient;

class RepositoriesClientTest extends AbstractClientTest {
  
  public function getClient() {
    $description = Bitbucket::getDescription('repositories');
    $client = new Client();
    return new BitbucketClient($client, $description);
  }

  public function testGetRepositories() {
    $command = $this->client->getCommand('getRepositories', ['owner' => 'foo']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/', $command);
  }

  public function testGetRepository() {
    $command = $this->client->getCommand('getRepository', ['owner' => 'foo', 'repo_slug' => 'bar']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar', $command);
  }

  public function testDeleteRepository() {
    $command = $this->client->getCommand('deleteRepository', ['owner' => 'foo', 'repo_slug' => 'bar']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar', $command);
    $this->assertCommandMethod('DELETE', $command);
  }

  public function testCreateRepository() {
    $command = $this->client->getCommand('createRepository', ['owner' => 'foo', 'repo_slug' => 'bar', 'repository' => ['name' => 'Foo Bar']]);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar', $command);
    $this->assertCommandMethod('POST', $command);
    $this->assertCommandBody('{"repository":{"name":"Foo Bar"}}', $command);
  }

  public function testGetRepositoryForks() {
    $command = $this->client->getCommand('getRepositoryForks', ['owner' => 'foo', 'repo_slug' => 'bar']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/forks', $command);
  }

  public function testGetRepositoryWatchers() {
    $command = $this->client->getCommand('getRepositoryWatchers', ['owner' => 'foo', 'repo_slug' => 'bar']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/watchers', $command);
  }

  public function testGetRepositoryDiff() {
    $command = $this->client->getCommand('getRepositoryDiff', ['owner' => 'foo', 'repo_slug' => 'bar', 'spec' => 'abc']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/diff/abc', $command);
  }

  public function testGetRepositoryPatch() {
    $command = $this->client->getCommand('getRepositoryPatch', ['owner' => 'foo', 'repo_slug' => 'bar', 'spec' => 'abc']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/patch/abc', $command);
  }
}