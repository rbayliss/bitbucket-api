<?php

namespace rbayliss\BitbucketAPI\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Command\Guzzle\GuzzleClient;
use rbayliss\BitbucketAPI\Bitbucket;
use rbayliss\BitbucketAPI\BitbucketClient;

class CommitsClientTest extends AbstractClientTest {

  public function getClient() {
    $description = Bitbucket::getDescription('commits');
    $client = new Client();
    return new BitbucketClient($client, $description);
  }

  public function testGetCommits() {
    $command = $this->client->getCommand('getCommits', ['owner' => 'foo', 'repo_slug' => 'bar']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/commits/', $command);
  }

  public function testGetCommitsOnBranch() {
    $command = $this->client->getCommand('getCommits', ['owner' => 'foo', 'repo_slug' => 'bar', 'branchortag' => 'master']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/commits/master', $command);
  }

  public function testGetCommitsWithExclude() {
    $command = $this->client->getCommand('getCommits', ['owner' => 'foo', 'repo_slug' => 'bar', 'exclude' => 'master']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/commits/?exclude=master', $command);
  }

  public function testGetCommitsWithInclude() {
    $command = $this->client->getCommand('getCommits', ['owner' => 'foo', 'repo_slug' => 'bar', 'include' => 'master']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/commits/?include=master', $command);
  }

  public function testGetCommit() {
    $command = $this->client->getCommand('getCommit', ['owner' => 'foo', 'repo_slug' => 'bar', 'revision' => 'ac34a']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/commit/ac34a', $command);
  }

  public function testGetCommitComments() {
    $command = $this->client->getCommand('getCommitComments', ['owner' => 'foo', 'repo_slug' => 'bar', 'revision' => 'ac34a']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/commit/ac34a/comments', $command);
  }

  public function testGetCommitComment() {
    $command = $this->client->getCommand('getCommitComment', ['owner' => 'foo', 'repo_slug' => 'bar', 'revision' => 'ac34a', 'comment_id' => 123]);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/commit/ac34a/comments/123', $command);
  }

  public function testCreateCommitApproval() {
    $command = $this->client->getCommand('createCommitApproval', ['owner' => 'foo', 'repo_slug' => 'bar', 'revision' => 'ac34a']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/commit/ac34a/approve', $command);
    $this->assertCommandMethod('POST', $command);
  }

  public function testDeleteCommitApproval() {
    $command = $this->client->getCommand('deleteCommitApproval', ['owner' => 'foo', 'repo_slug' => 'bar', 'revision' => 'ac34a']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/repositories/foo/bar/commit/ac34a/approve', $command);
    $this->assertCommandMethod('DELETE', $command);
  }
}