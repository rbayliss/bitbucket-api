<?php

namespace rbayliss\BitbucketAPI\Tests;

use GuzzleHttp\Command\CommandInterface;
use GuzzleHttp\Command\CommandTransaction;
use GuzzleHttp\Command\Event\InitEvent;
use GuzzleHttp\Command\Guzzle\Serializer;

abstract class AbstractClientTest extends \PHPUnit_Framework_TestCase {

  /**
   * @var \GuzzleHttp\Command\Guzzle\GuzzleClient
   */
  protected $client;

  public function setUp() {
    $this->client = $this->getClient();
  }

  /**
   * @return \GuzzleHttp\Command\Guzzle\GuzzleClient
   */
  abstract protected function getClient();

  protected function assertCommandUrl($url, CommandInterface $command) {
    $trans = new CommandTransaction($this->client, $command);
    // Trigger validation:
    $command->getEmitter()->emit('init', new InitEvent($trans));
    $s = new Serializer($this->client->getDescription());
    $request = $s($trans);
    $this->client->getEmitter();
    $this->assertEquals($url, (string) $request->getUrl());
  }

  protected function assertCommandMethod($method, CommandInterface $command) {
    $trans = new CommandTransaction($this->client, $command);
    // Trigger validation:
    $command->getEmitter()->emit('init', new InitEvent($trans));
    $s = new Serializer($this->client->getDescription());
    $request = $s($trans);
    $this->assertEquals($method, (string) $request->getMethod());
  }

  protected function assertCommandBody($body, CommandInterface $command) {
    $trans = new CommandTransaction($this->client, $command);
    // Trigger validation:
    $command->getEmitter()->emit('init', new InitEvent($trans));
    $s = new Serializer($this->client->getDescription());
    /** @var \GuzzleHttp\Message\Request $request */
    $request = $s($trans);
    $this->assertEquals($body, $request->getBody()->getContents());
  }

}