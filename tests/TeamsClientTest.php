<?php

namespace rbayliss\BitbucketAPI\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Command\Guzzle\GuzzleClient;
use rbayliss\BitbucketAPI\Bitbucket;
use rbayliss\BitbucketAPI\BitbucketClient;

class TeamsClientTest extends AbstractClientTest {
  
  public function getClient() {
    $description = Bitbucket::getDescription('teams');
    $client = new Client();
    return new BitbucketClient($client, $description);
  }

  public function testGetTeam() {
    $command = $this->client->getCommand('getTeam', ['teamname' => 'lastcall']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/teams/lastcall', $command);
  }

  public function testGetTeamFollowers() {
    $command = $this->client->getCommand('getTeamFollowers', ['teamname' => 'lastcall']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/teams/lastcall/followers', $command);
  }

  public function testGetTeamFollowing() {
    $command = $this->client->getCommand('getTeamFollowing', ['teamname' => 'lastcall']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/teams/lastcall/following', $command);
  }

  public function testGetTeamRepositories() {
    $command = $this->client->getCommand('getTeamRepositories', ['teamname' => 'lastcall']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/teams/lastcall/repositories', $command);
  }
}