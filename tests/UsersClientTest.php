<?php

namespace rbayliss\BitbucketAPI\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Command\Guzzle\GuzzleClient;
use rbayliss\BitbucketAPI\Bitbucket;
use rbayliss\BitbucketAPI\BitbucketClient;

class UsersClientTest extends AbstractClientTest {

  public function getClient() {
    $description = Bitbucket::getDescription('users');
    $client = new Client();
    return new BitbucketClient($client, $description);
  }

  public function testGetUser() {
    $command = $this->client->getCommand('getUser', ['username' => 'rbayliss']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/users/rbayliss', $command);
  }

  public function testGetUserFollowers() {
    $command = $this->client->getCommand('getUserFollowers', ['username' => 'rbayliss']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/users/rbayliss/followers', $command);
  }

  public function testGetUserFollowing() {
    $command = $this->client->getCommand('getUserFollowing', ['username' => 'rbayliss']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/users/rbayliss/following', $command);
  }

  public function testGetUserRepositories() {
    $command = $this->client->getCommand('getUserRepositories', ['username' => 'rbayliss']);
    $this->assertCommandUrl('https://bitbucket.org/api/2.0/users/rbayliss/repositories', $command);
  }
}