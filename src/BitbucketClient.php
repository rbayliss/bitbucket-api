<?php

namespace rbayliss\BitbucketAPI;

use GuzzleHttp\Command\Guzzle\GuzzleClient;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

class BitbucketClient extends GuzzleClient {

  protected $credentials;

  protected function processConfig(array $config) {
    parent::processConfig($config);
    if(!empty($config['login']) && is_array($config['login'])) {
      $this->getHttpClient()->setDefaultOption('auth', array_values($config['login']));
    }
    if(!empty($config['oauth'])) {
      $plugin = new Oauth1([
        'consumer_key' => $config['oauth']['key'],
        'consumer_secret' => $config['oauth']['secret'],
      ]);
      $this->getHttpClient()->getEmitter()->attach($plugin);
      $this->getHttpClient()->setDefaultOption('auth', 'oauth');
    }
  }
}