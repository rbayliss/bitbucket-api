<?php

namespace rbayliss\BitbucketAPI;

use GuzzleHttp\Client;
use GuzzleHttp\Collection;
use GuzzleHttp\Command\Guzzle\Description;

class Bitbucket {

  protected $httpClient;

  protected $config;

  public function __construct($config = array()) {
    $this->config = new Collection($config);
  }

  public static function getDescription($serviceName) {
    $filename = __DIR__ . '/Resources/' . $serviceName . '.php';
    if(file_exists($filename)) {
      $description = include $filename;
      $common = include __DIR__ . '/Resources/_common.php';
      foreach($common['models'] as $name => $model) {
        $description['models'][$name] = $model;
      }

      return new Description($description);
    }
    throw new \Exception('Description not found');
  }

  public function getHttpClient() {
    if (!$this->httpClient) {
      $this->httpClient = new Client();
    }

    return $this->httpClient;
  }

  public function setHttpClient(Client $httpClient) {
    $this->httpClient = $httpClient;
  }

  protected function getServiceClient($serviceName, $config = []) {
    $serviceConfig = clone $this->config;
    $serviceConfig->overwriteWith($config);
    return new BitbucketClient($this->getHttpClient(), self::getDescription($serviceName), $serviceConfig->toArray());
  }

  public function users($config = []) {
    return $this->getServiceClient('users', $config);
  }

  public function teams($config = []) {
    return $this->getServiceClient('teams', $config);
  }

  public function repositories($config = []) {
    return $this->getServiceClient('repositories', $config);
  }

  public function commits($config = []) {
    return $this->getServiceClient('commits', $config);
  }

  public function branchRestrictions($config = []) {
    return $this->getServiceClient('branch-restrictions', $config);
  }

}