<?php

return array(
  'baseUrl' => 'https://bitbucket.org/api/2.0/teams/',
  'operations' => [
    '_abstractTeam' => [
      'abstract' => TRUE,
      'uri' => '{teamname}',
      'httpMethod' => 'GET',
      'parameters' => [
        'teamname' => [
          'required'  => true,
          'type'      => 'string',
          'location'  => 'uri'
        ],
      ],
    ],
    'getTeam' => [
      'extends' => '_abstractTeam',
      'responseClass' => 'UserAccount',
    ],
    'getTeamMembers' => [
      'uri' => '{teamname}/members',
      'extends' => '_abstractTeam',
      'responseClass' => 'UserAccountList',
    ],
    'getTeamFollowers' => array(
      'uri' => '{teamname}/followers',
      'extends' => '_abstractTeam',
      'responseClass' => 'UserAccountList',
    ),
    'getTeamFollowing' => [
      'uri' => '{teamname}/following',
      'extends' => '_abstractTeam',
      'responseClass' => 'UserAccountList',
    ],
    'getTeamRepositories' => [
      'uri' => '{teamname}/repositories',
      'extends' => '_abstractTeam',
      'responseClass' => 'RepositoriesList',
    ]
  ]
);