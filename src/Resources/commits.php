<?php

return array(
  'baseUrl' => 'https://bitbucket.org/api/2.0/repositories/',
  'operations' => [
    'getCommits' => [
      'uri' => '{owner}/{repo_slug}/commits/{branchortag}',
      'httpMethod' => 'GET',
      'responseClass' => 'CommitList',
      'parameters' => [
        'owner' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'repo_slug' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'branchortag' => [
          'type' => 'string',
          'location' => 'uri',
        ],
        'exclude' => [
          'type' => 'string',
          'location' => 'query',
        ],
        'include' => [
          'type' => 'string',
          'location' => 'query',
        ]
      ],
    ],
    'getCommit' => [
      'uri' => '{owner}/{repo_slug}/commit/{revision}',
      'httpMethod' => 'GET',
      'responseClass' => 'Commit',
      'parameters' => [
        'owner' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'repo_slug' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'revision' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri',
        ],
      ]
    ],
    'getCommitComments' => [
      'uri' => '{owner}/{repo_slug}/commit/{revision}/comments',
      'httpMethod' => 'GET',
      'responseClass' => 'GenericResponse',
      'parameters' => [
        'owner' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'repo_slug' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'revision' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri',
        ],
      ]
    ],
    'getCommitComment' => [
      'uri' => '{owner}/{repo_slug}/commit/{revision}/comments/{comment_id}',
      'httpMethod' => 'GET',
      'responseClass' => 'CommitComment',
      'parameters' => [
        'owner' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'repo_slug' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'revision' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri',
        ],
        'comment_id' => [
          'required' => TRUE,
          'type' => 'numeric',
          'location' => 'uri',
        ]
      ]
    ],
    'createCommitApproval' => [
      'uri' => '{owner}/{repo_slug}/commit/{revision}/approve',
      'httpMethod' => 'POST',
      'responseClass' => 'CommitApproval',
      'parameters' => [
        'owner' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'repo_slug' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'revision' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri',
        ]
      ]
    ],
    'deleteCommitApproval' => [
      'extends' => 'createCommitApproval',
      'httpMethod' => 'DELETE',
      'responseClass' => 'GenericResponse',
    ]
  ]
);