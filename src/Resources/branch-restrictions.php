<?php

return array(
  'baseUrl' => 'https://bitbucket.org/api/2.0/repositories/',
  'operations' => [
    'getBranchRestrictions' => [
      'uri' => '{owner}/{repo_slug}/branch-restrictions',
      'httpMethod' => 'GET',
      'responseClass' => 'BranchRestrictionList',
      'parameters' => [
        'owner' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'repo_slug' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
      ]
    ],
    'getBranchRestriction' => [
      'uri' => '{owner}/{repo_slug}/branch-restrictions/{restriction_id}',
      'httpMethod' => 'GET',
      'responseClass' => 'BranchRestrictionList',
      'parameters' => [
        'owner' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'repo_slug' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'restriction_id' => [
          'required' => TRUE,
          'type' => 'numeric',
          'location' => 'uri',
        ]
      ]
    ],
    'updateBranchRestriction' => [
      'extends' => 'getBranchRestriction',
      'httpMethod' => 'PUT',
      'responseClass' => 'GenericResponse',
      'parameters' => [
        'owner' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'repo_slug' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'restriction_id' => [
          'required' => TRUE,
          'type' => 'numeric',
          'location' => 'uri',
        ],
        'groups' => [
          'location' => 'json',
          'type' => 'object',
          'type' => 'array',
          'items' =>[
            '$ref' => 'Group',
          ]
        ],
        'users' => [
          'location' => 'json',
          'type' => 'object',
          'type' => 'array',
          'items' => [
            'username' => [
              'type' => 'string',
            ]
          ]
        ],
        'pattern' => [
          'location' => 'json',
          'type' => 'string',
        ]
      ]
    ],
    'deleteBranchRestriction' => [
      'extends' => 'getBranchRestriction',
      'httpMethod' => 'DELETE',
      'responseClass' => 'GenericResponse',
    ],
    'createBranchRestriction' => [
      'uri' => '{owner}/{repo_slug}/branch-restrictions',
      'responseClass' => 'GenericResponse',
      'httpMethod' => 'POST',
      'parameters' => [
        'owner' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'repo_slug' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri'
        ],
        'groups' => [
          'location' => 'body',
          'type' => 'array',
        ],
        'kind' => [
          'location' => 'json',
          'type' => 'string',
        ],
        'pattern' => [
          'location' => 'json',
          'type' => 'string',
        ],
        'users' => [
          'location' => 'json',
          'type' => 'array',
          'items' => [
            'username' => [
              'type' => 'string',
            ]
          ]
        ],
      ]
    ],

  ]
);
