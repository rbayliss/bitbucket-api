<?php

return array(
  'models' => array(
    'GenericResponse' => [
      'type' => 'object',
      'additionalProperties' => [
        'location' => 'json',
      ]
    ],
    'RepositoriesList' => [
      'type' => 'object',
      'location' => 'json',
      'properties' => [
        'pagelen' => [
          'type' => 'numeric',
        ],
        'page' => [
          'type' => 'numeric',
        ],
        'size' => [
          'type' => 'numeric',
        ],
        'values' => [
          'type' => 'array',
          'items' => [
            '$ref' => 'Repository',
          ]
        ]
      ],
    ],
    'UserAccountList' => [
      'type' => 'object',
      'location' => 'json',
      'properties' => [
        'pagelen' => [
          'type' => 'numeric',
        ],
        'page' => [
          'type' => 'numeric',
        ],
        'size' => [
          'type' => 'numeric',
        ],
        'values' => [
          'type' => 'array',
          'items' => [
            '$ref' => 'UserAccount',
          ]
        ]
      ],
    ],
    'CommitList' => [
      'type' => 'object',
      'location' => 'json',
      'properties' => [
        'pagelen' => [
          'type' => 'numeric',
        ],
        'page' => [
          'type' => 'numeric',
        ],
        'size' => [
          'type' => 'numeric',
        ],
        'values' => [
          'type' => 'array',
          'items' => [
            '$ref' => 'Commit',
          ]
        ]
      ],
    ],
    'CommitCommentList' => [
      'type' => 'object',
      'location' => 'json',
      'properties' => [
        'pagelen' => [
          'type' => 'numeric',
        ],
        'page' => [
          'type' => 'numeric',
        ],
        'size' => [
          'type' => 'numeric',
        ],
        'values' => [
          'type' => 'array',
          'items' => [
            '$ref' => 'CommitComment',
          ]
        ]
      ],
    ],
    'BranchRestrictionList' => [
      'type' => 'object',
      'location' => 'json',
      'properties' => [
        'pagelen' => [
          'type' => 'numeric',
        ],
        'page' => [
          'type' => 'numeric',
        ],
        'size' => [
          'type' => 'numeric',
        ],
        'values' => [
          'type' => 'array',
          'items' => [
            '$ref' => 'BranchRestriction',
          ]
        ]
      ],
    ],
    'Repository' => [
      'type' => 'object',
      'location' => 'json',
      'parameters' => [
        'scm' => [
          'type' => 'string',
          'enum' => ['git', 'hg'],
        ],
        'name' => [
          'type' => 'string',
        ],
        'is_private' => [
          'type' => 'boolean'
        ],
        'description' => [
          'type' => 'string'
        ],
        'fork_policy' => [
          'type' => 'string',
          'enum' => ['allow_forks', 'no_public_forks', 'no_forks']
        ],
        'language' => [
          'type' => 'string',
          'filter' => 'strtolower',
        ],
        'has_issues' => [
          'type' => 'boolean',
        ],
        'has_wiki' => [
          'type' => 'boolean',
        ]
      ],
      'properties' => [
        'uuid' => [
          'type' => 'string',
        ],
        'name' => [
          'type' => 'string',
        ],
        'full_name' => [
          'type' => 'string',
        ],
        'description' => [
          'type' => 'string',
        ],
        'owner' => [
          'type' => 'object',
          '$ref' => 'UserAccount',
        ],
        'created_on' => [
          'type' => 'string',
          'format' => 'date-time-http'
        ],
        'updated_on' => [
          'type' => 'string',
          'format' => 'date-time-http'
        ],
        'size' => [
          'type' => 'numeric',
        ],
        'parent' => [
          'type' => 'object',
          '$ref' => 'Repository',
        ],
        'language' => [
          'type' => 'string',
        ],
        'scm' => [
          'type' => 'string',
          'enum' => ['git', 'hg'],
        ],
        'has_wiki' => [
          'type' => 'boolean'
        ],
        'has_issues' => [
          'type' => 'boolean',
        ],
        'is_private' => [
          'type' => 'boolean',
        ],
        'links' => [
          'type' => 'object',
          'items' => [
            'type' => 'object',
          ]
        ],
        'fork_policy' => [
          'type' => 'string',
          'enum' => ['allow_forks', 'no_public_forks', 'no_forks']
        ],
      ],
    ],
    'UserAccount' => [
      'type' => 'object',
      'location' => 'json',
      'properties' => [
        'uuid' => [
          'type' => 'string',
        ],
        'username' => [
          'type' => 'string',
        ],
        'type' => [
          'type' => 'string',
        ],
        'website' => [
          'type' => 'string',
        ],
        'display_name' => [
          'type' => 'string',
        ],
        'links' => [
          'type' => 'object',
        ],
        'created_on' => [
          'type' => 'string',
        ],
        'location' => [
          'type' => 'string',
        ]
      ],
    ],
    'Commit' => [
      'type' => 'object',
      'location' => 'json',
      'properties' => [
        'hash' => [
          'type' => 'string',
        ],
        'repository' => [
          'type' => 'object',
          '$ref' => 'Repository',
        ],
        'links' => [
          'type' => 'object',
        ],
        'author' => [
          '$ref' => 'UserAccount',
        ],
        'date' => [
          'type' => 'string',
          'format' => 'date-time-http'
        ],
        'message' => [
          'type' => 'string',
        ],
        'parents' => [
          'type' => 'array',
          'items' => [
            '$ref' => 'Commit',
          ],
        ]
      ]
    ],
    'CommitComment' => [
      'type' => 'object',
      'location' => 'json',
      'properties' => [
        'id' => [
          'type' => 'numeric',
        ],
        'created_on' => [
          'type' => 'string',
          'format' => 'date-time-http'
        ],
        'updated_on' => [
          'type' => 'string',
          'format' => 'date-time-http'
        ],
        'user' => [
          'type' => 'object',
          '$ref' => 'UserAccount',
        ],
        'content' => [
          'type' => 'object',
          // Has additional properties
        ],
        'inline' => [
          'type' => 'object',
          // Has additional properties
        ],
        'links' => [
          'type' => 'object',
          // Has additional properties
        ]
      ]
    ],
    'CommitApproval' => [
      'type' => 'object',
      'location' => 'json',
      'properties' => [
        'role' => [
          'type' => 'string',
        ],
        'user' => [
          'type' => 'object',
          '$ref' => 'UserAccount',
        ],
        'approved' => [
          'type' => 'boolean',
        ]
      ]
    ],
    'BranchRestriction' => [
      'type' => 'object',
      'location' => 'json',
      'properties' => [
        'id' => [
          'type' => 'numeric',
        ],
        'kind' => [
          'type' => 'string',
        ],
        'groups' => [
          'type' => 'array',
          'items' => [
            '$ref' => 'Group',
          ]
        ],
        'pattern' => [
          'type' => 'string',
        ],
        'users' => [
          'type' => 'array',
          'items' => [
            '$ref' => 'UserAccount',
          ]
        ],
        'links' => [
          'type' => 'object',
        ],
      ]
    ],
    'Group' => [
      'type' => 'object',
      'location' => 'json',
      'properties' => [
        'name' => [
          'type' => 'string',
        ],
        'account_privilege' => [
          'type' => 'string',
        ],
        'slug' => [
          'type' => 'string',
        ],
        'full_slug' => [
          'type' => 'string',
        ],
        'members' => [
          'type' => 'numeric',
        ],
        'owner' => [
          '$ref' => 'UserAccount',
        ],
        'links' => [
          'type' => 'object',
        ],
      ]
    ]
  )
);