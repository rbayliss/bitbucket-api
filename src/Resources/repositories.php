<?php

return array(
  'baseUrl' => 'https://bitbucket.org/api/2.0/repositories/',
  'operations' => [
    '_abstractRepositories' => [
      'abstract' => TRUE,
      'uri' => '{owner}/{repo_slug}',
      'httpMethod' => 'GET',
      'parameters' => [
        'owner' => [
          'required'  => true,
          'type'      => 'string',
          'location'  => 'uri'
        ],
        'repo_slug' => [
          'required'  => true,
          'type'      => 'string',
          'location'  => 'uri'
        ],
      ],
      'responseModel' => 'RepositoryResponse',
    ],
    '_abstractRepositoriesSpec' => [
      'abstract' => TRUE,
      'parameters' => [
        'owner' => [
          'required'  => true,
          'type'      => 'string',
          'location'  => 'uri'
        ],
        'repo_slug' => [
          'required'  => true,
          'type'      => 'string',
          'location'  => 'uri'
        ],
        'spec' => [
          'required' => TRUE,
          'type' => 'string',
          'location' => 'uri',
        ]
      ]
    ],
    'getRepositories' => [
      'extends' => '_abstractRepositories',
      'responseModel' => 'RepositoriesList',
      'parameters' => [
        'owner' => [
          'required'  => false,
          'type'      => 'string',
          'location'  => 'uri'
        ],
        'repo_slug' => [
          'required'  => false,
          'type'      => 'string',
          'location'  => 'uri'
        ],
      ]
    ],
    'getRepository' => [
      'extends' => '_abstractRepositories',
      'responseModel' => 'Repository',
    ],
    'deleteRepository' => [
      'extends' => '_abstractRepositories',
      'httpMethod' => 'DELETE',
    ],
    'createRepository' => [
      'extends' => '_abstractRepositories',
      'httpMethod' => 'POST',
      'parameters' => [
        'owner' => [
          'required'  => true,
          'type'      => 'string',
          'location'  => 'uri'
        ],
        'repo_slug' => [
          'required'  => true,
          'type'      => 'string',
          'location'  => 'uri'
        ],
        'repository' => [
          'required' => true,
          '$ref' => 'Repository',
        ]
      ]
    ],
    'getRepositoryForks' => [
      'extends' => '_abstractRepositories',
      'uri' => '{owner}/{repo_slug}/forks',
      'responseModel' => 'RepositoriesList',
    ],
    'getRepositoryWatchers' => [
      'extends' => '_abstractRepositories',
      'uri' => '{owner}/{repo_slug}/watchers'
    ],
    'getRepositoryDiff' => [
      'extends' => '_abstractRepositoriesSpec',
      'uri' => '{owner}/{repo_slug}/diff/{spec}',
    ],
    'getRepositoryPatch' => [
      'extends' => '_abstractRepositoriesSpec',
      'uri' => '{owner}/{repo_slug}/patch/{spec}',
    ]
  ],
  'models' => [
    'RepositoryResponse' => [
      'type' => 'object',
      'additionalProperties' => [
        'location' => 'json',
      ]
    ],
  ]
);