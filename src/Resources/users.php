<?php

return array(
  'baseUrl' => 'https://bitbucket.org/api/2.0/users/',
  'operations' => [
    '_abstractUser' => [
      'abstract' => TRUE,
      'uri' => '{username}',
      'httpMethod' => 'GET',
      'parameters' => [
        'username' => [
          'required'  => true,
          'type'      => 'string',
          'location'  => 'uri'
        ],
      ],
    ],
    'getUser' => [
      'extends' => '_abstractUser',
      'responseClass' => 'UserAccount',
    ],
    'getUserFollowers' => array(
      'uri' => '{username}/followers',
      'extends' => '_abstractUser',
      'responseClass' => 'UserAccountList',
    ),
    'getUserFollowing' => [
      'uri' => '{username}/following',
      'extends' => '_abstractUser',
      'responseClass' => 'UserAccountList',
    ],
    'getUserRepositories' => [
      'uri' => '{username}/repositories',
      'extends' => '_abstractUser',
      'responseClass' => 'RepositoriesList',
    ]
  ]
);